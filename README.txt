Webform Ajax Validation
=======================
This module provides simple ajax integration between the clientside_validation module and arbitrary validators
written for the webform_validation module.  It does nothing on its own, and is intended only to
simplify the process of validating webform components via ajax.

Clientside Validation provides javascript implementations of many Webform Validation validators, and in general
this is far preferable to validating via ajax.  However, there are some cases where validation must
be done on the server side (e.g. those involving web-services with private API keys).  This module allows
you to perform those validations via ajax, seamlessly integrating them into an existing clientside
validation user experience.

Usage
=====
Any Webform Validation validator can be "ajax-enabled" simply by adding an 'ajax' => TRUE key to to the array
returned by hook_webform_validation_validators().  For example, an existing validator could be enabled as:

  function mymodule_webform_validator_alter($validators) {
    $validators['the_one_to_enable'] += array('ajax' => TRUE);
  }

That's all you need to do. Once enabled, any components having a rule using that validator will be validated
via ajax, unless they are specifically excluded from clientside validation (via the checkbox on the component
edit form).


Notes
=====
- Components validated via ajax will be validated a second time when the form is actually submitted. If you wish
to avoid this, you must implement hook_webform_validation_validate() so that it only executes once -- for
example:

  function mymodule_webform_validation_validate($validator_name, $items, $components, $rule) {
    // Prevent this from running twice - once on ajax and then again on form submission.
    if (function_exists('_webform_ajax_validation_enabled') && _webform_ajax_validation_enabled('active_phone', $component) && $_GET['q'] !== 'webform_ajax_validation') return;
    ...
  }

Note that this will not be possible for existing validators.


Known Issues
============
- If you have more than one ajax validator on the same component, you cannot currently
set a different custom error message for each (this will hopefully be fixed in the future).

- If you have more than one component in the same form validated via ajax, there will be a
separate ajax call for each validation.

- Ajax calls are synchronous, so you cannot display a throbber.