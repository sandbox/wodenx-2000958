/**
 * @file
 * Webform Ajax Validation.
 * Adds a custom clientside_validation validator to validate components via ajax.
 */
(function ($) {
  //Define a Drupal behaviour with a custom name
  Drupal.behaviors.webformAjaxValidation = {
    attach: function (context) {
      function doValidation(value, element, param) {
    	var result = true;
    	// Add a keydown handler to signal that a keystroke is in progress on this element
    	// because we don't want to ping the back-end on every keystroke!
    	if (!$(element).data('webform-ajax-validation-processed')) {
    		$(element).data('webform-ajax-validation-processed', 1);
    		$(element).keydown(function(evt) {
    			if (evt.keyCode !== 13) { // Don't suspend validation on enter key.
    				$(element).data('webform-ajax-validation-is-keystroke', 1);
    			}
    			return true;
    		});
    	}
    	// Only do the back-end validation if this is not a keyup event.
    	if (!$(element).data('webform-ajax-validation-is-keystroke')) {
    		var form_id = $(element).closest('form').find('input[name=form_id]').val();
    		//var throbber = $('<span class="ajax-progress-throbber">*</span>');
    		//$(element).before(throbber); // Doesn't work bc this is a synchronous ajax call.
    		jQuery.ajax({
    			'url': Drupal.settings.basePath + 'webform_ajax_validation',
    			'type': "POST",
    			'data': {
    				'value': value,
    				'name': $(element).attr('name'),
    				'form_id': form_id,
    			},
    			'dataType': 'json',
    			'async': false,
    			'success': function(res){
    				result = res.result;
    			}
    		});
    	}
      //$(throbber).remove();
    	$(element).removeData('webform-ajax-validation-is-keystroke');
        return result;
      }

      // Add an eventlistener to the document reacting on the
      // 'clientsideValidationAddCustomRules' event.
      $(document).bind('clientsideValidationAddCustomRules', function(event) {
    	  // !TODO Add a separate copy of the validation function for each rule?
	      jQuery.validator.addMethod("webformAjaxValidation", doValidation, jQuery.format('Invalid entry.'));
      });
    }
  }
})(jQuery);
